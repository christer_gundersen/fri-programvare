## Digital delingskultur og sirkulær økonomi

Ved inngangen til et nytt år har jeg reflektert litt rundt hvordan den digitale delingskulturen og utviklingen av den frie delen av internett har forandret verden helt grunnleggende de siste 30 årene. Da Linus Torvalds publiserte den første versjonen av Linux i 1991 markerte det starten på en fantastiske reise. Fri programvare har siden gått fra å være en undervurdert subkultur til å bli den dominerende ingrediensen for utvikling av infrastruktur i de aller fleste digitale sektorer og industrier. 

Jeg har lenge vært interessert i å lære mer om de faktorene og “næringsstoffene” som skaper gode vekstvilkår i de frie digitale vassdragene og biotopene som sammen danner det vi idag kaller den digitale allmenningen. 

Hvem er de som bidrar, hvem høster fruktene av delingen og hvilke fellesnevnere finner vi igjen i prosjekter og organisasjoner som lykkes? Hva motiverer oss til å dele kildekode, bilder, kunst og andre digitale verk? Hvordan finner mennesker, organisasjoner og bedrifter med så forskjellige utgangspunkt sin plass i dette mylderet av prosjekter, plattformer og fagområder? Det ligger mye nyttig læring og innovasjon i forstå denne dynamikken, både for privat og offentlig sektor.

Dette handler ikke lenger bare om teknologi og programvare. Forskere, artister, kunstnere og lærere er bare noen av yrkesgruppene som i økende grad bidrar gjennom å dele det de skaper. Dette har ført til at innhold som er delt på den frie delen av internett har økt eksponentielt de siste årene. Antallet verk delt under en fri lisens passerte 1 milliard i 2016. I skrivende stund, bare 5 år senere, estimerer Creative Commons at tallet har passert 2 milliarder.

Etter å ha vært engasjert i fri programvare og fri kultur det meste av min yrkesaktive karriere ser jeg noen fellestrekk og mønster. Disse digitale vassdragene og biotopene har et godt jordsmonn for deling. Dette jordsmonnet består av en mengde faktorer, men noen går ofte igjen:
* Digital deling må være lovlig og de som skal dele må skjønne hvordan de deler riktig 
* De som bidrar må være motivert til å dele. De må se nytten, enten for seg selv, eller for andre. Dette gjelder enten de er frivillige bidragsytere, offentlig ansatte eller kommersielle bedrifter  
* Det må være rom for forskjellige typer bidrag fra mennesker med ulik kompetanse og bakgrunn - delingskulturen er bygget på mer enn bare teknologi
* De som bidrar må kunne dele i en kontekst som er relevant for dem 

## Vi deler fordi det er mulig
Det aller viktigste når man skal skape et godt jordsmonn for deling er at man har anledning til å dele på en lovlig, ryddig og enkel måte. Her er lisensene for programvare, læringsressurser, data, forskning, kunst og kultur helt avgjørende. 
I den innledende fasen av internett var delingskulturen sentrert rundt fri programvare og teknologi. 

Dette er grunnen til at de aller fleste lisensene som brukes i dag er basert på, eller inspirert av, prinsippene som ble utformet av Richard Stallman og Free Software Foundation (FSF) på 1980-tallet. Stallman og FSF utformet noen enkle prinsipper som siden har blitt kalt de fire frihetene:
* Friheten til å kjøre programmet som du ønsker, uansett hensikt
* Friheten til å studere hvordan programmet virker (fordi en har tilgang og innsyn i kildekoden uten begrensninger), og endre det slik at det utfører dine beregninger slik du ønsker
* Friheten til å videredistribuere kopier
* Friheten til å distribuere kopier av dine endrede versjoner til andre

Disse prinsippene og lisensene som kom fra FSF har siden dannet grunnlaget for en rekke andre lisenser som dekker fagområder utover bare teknologi. Et eksempel er Creative Commons lisensene, som ble utviklet av fri kultur bevegelsen tidlig på 2000-tallet. Disse brukes idag til lisensiering av alt fra kunst, musikk, bilder, data og offentlige dokumenter.
Hva motiverer oss til å dele?

Det er ikke nok bare å kunne dele lovlig. De som deler må være motivert og de må forstå mulighetene som ligger i å jobbe sammen på nye måter. Drivkraften til bidragsyterne i denne delingskulturen er veldig ulike. Motivasjonen for de som bidrar i en ren digital dugnad vil naturligvis være annerledes en de som ønsker å tjene til livets opphold samtidig som de deler. Offentlig sektor på sin side motiveres av andre faktorer enn private bedrifter og har mulighet til å samhandle på andre måter. 
Digital dugnad

Den digitale dugnaden er kjernen i den digitale allmenningen. På samme måte som frivillige bidrar til å drifte en basketballklubb, en turisthytte eller et sangkor er det nå flere og flere som bruker sin fritid på den digitale versjonen av dugnaden. Noen bidrar på Wikipedia ved å skrive om et tema de brenner for, andre sanker inn kartdata for tjenester som OpenStreetMap. En veldig stor gruppe bidragsytere finner et programvare-prosjekt de brenner for, og legger sine dugnadstimer i å skrive eller teste kode. 

Det som skiller den digitale dugnaden fra den fysiske er at den skalerer i et “digitalt paradigme”. Hvis for eksempel en møbelsnekker bygger 10 stoler til en DNT hytte, vil dette være et fantastisk bidrag til akkurat denne hytten. Disse stolene kan imidlertid kun brukes på denne hytten, og flytter man en stol til en annen hytte vil det være en mindre tilgjengelig på den opprinnelige. 

Dugnadsbidrag i det digitale paradigmet er ikke underlagt de samme begrensningene. Hadde disse stolene vært digitale kunne man enkelt kjørt “copy and paste” og latt andre bruke dem, og kostnaden ved kopiering og frakt er nær null. Hvis for eksempel en forsker bidrar til en artikkel på Wikipedia kan den potensielt leses av mange millioner mennesker. Andre kan også lage sine egne versjoner av den opprinnelige artikkelen, for eksempel gjennom oversettelse. Dette skjer helt uten at den opprinnelige artikkelen påvirkes. Slik kan digitale bidrag nå så uendelig mange flere mennesker sammenlignet med de fysiske. Dette er nok også med å motivere mange som bidrar til digital dugnad. 

De som forstår denne grunnleggende forskjellen lykkes oftere. De som forholder seg til det digitale som om at det er fysisk, lykkes ikke med å utvikle digital delingskultur. Norsk offentlig sektor er et eksempel på det siste. 

## Digital sirkulær økonomi
I økende grad over de siste ti årene har kommersielle bedrifter, små og veldig store, funnet sin plass. De deler mer og bruker mer fri programvare, noe som fremmer bærekraftig utvikling både for dem selv og andre. 

I den kommersielle delen av den digitale allmenningen handler det ofte om en form for digital sirkulær økonomi. I stedet for kun å hente ut verdi, tilfører man noe hver gang man deler. Digitaliserte ressurser blir ikke oppbrukt men blir forbedret og tilpasset lokale forhold og behov. Dette gjør at man kan tenke nytt når man skal sikre inntekter til et selskap som både skal tjene på og bidra til dette økosystemet. 

Selskaper som Google, Facebook, Twitter, Amazon og Microsoft har de siste årene forståtte kraften som ligger i denne digitale sirkulær økonomien og bygger mange av sine produkter på fri programvare. Ja, du leste riktig, Microsoft er med i listen. Det handler mye om at disse selskapene har en stor portefølje med produkter som de tilbyr som skytjenester hvor fri programvare er en viktig del av grunnmuren. 

IBM og Microsoft er eksempler på selskaper som nå satser alt hva remmer og tøy kan holde på fri programvare. Da IBM i 2018 kjøpte Red Hat for vanvittige 284 milliarder norske kroner verdsatte de et selskap som all hovedsak er tuftet på fri programvare. Da Microsoft i 2016 kjøpte Github var det mange som ble svært overrasket. For Microsoft selv var det helt naturlig, etter at selskapet hadde vært gjennom et strategisk skifte hvor fri programvare var blitt en helt sentral del av selskapets fremtidige forretningsmodell. Microsoft betalte over 60 milliarder kroner for GitHub.

Motivasjonen for både Microsoft og IBM er blant annet knyttet til metodikken rundt utvikling av fri programvare. Økosystemet og metodene sikrer dem teknologi til en lavere kostnad og med høyere kvalitet. De bidrar samtidig til en digital sirkulær økonomi som er langt mer effektivt enn om hvert av disse selskapene skulle fortsatt å utviklet hele sin “stack” og infrastruktur fra bunnen av. 

I denne sirkulære økonomien er deling og fritt innsyn helt sentralt fordi det legger til rette for at flere smarte hoder kan jobbe sammen for å løse kompliserte problemer, noe som har vist seg å være bærekraftig selv for selskaper som har mye ressurser. 

Et helt konkret eksempel på dette er Google. Ved flere anledninger har de lyst ut konkurranser hvor det utloves en dusør, som utbetales til den/de som finner alvorlige feil i Googles kildekode. På denne måten brukes delingskulturen til å rette feil som Google ikke har kapasitet til å løse internt. Kunne offentlige sektor i Norge hatt gevinster av å gjort noe lignende?

Den kollektive effekten vil selvsagt være enda større for små selskaper med mindre kapasitet. En startup kan i dag bygge sin digitale infrastruktur på teknologi som andre har kvalitetssikret og utviklet gjennom fri programvare. Det eksisterer i dag en rekke forretningsmodeller som teknologiselskaper, forskere, artister, kunstnere og forfattere bruker for å tjene penger samtidig som man deler under en fri lisens. Det enkelte selskap eller organisasjon kan skape sitt “vassdrag”, hvor de utvikler sin forretningsmodell gjennom deling og gjenbruk. 

## Mennesker ønsker å dele
Enten man deler som ren dugnad eller for å utvikle en kommersiell virksomhet er det til syvende og sist enkeltmennesker som danner dette økosystemet. Det handler ofte om å bidra til noe som er større enn en selv. Det å kunne hjelpe andre gir rett og slett mening og det å bidra tilbake til et prosjekt man har hatt nytte av virker logisk og realt. Også de som utvikler en kommersiell virksomhet har ofte en klar visjon om at de ønsker å bidra til et bedre samfunn. For disse menneskene faller deling helt naturlig, både som metode, forretningsmodell og verdisyn. 

I denne teksten bruker jeg benevnelsen “den frie delen av internett” helt bevisst. Deling av bilder på Facebook, eller publisering av en hytte på AirBnB er ikke å regne som digital delingskultur. Facebook, Instagram og SnapChat fremmer “plattform kultur” hvor delingen i stor grad gjøres på plattformens premisser og hvor delingen ofte kommer med skjulte kostnader, også for de som bidrar til deling. Nettverkseffekten på disse plattformene er noen ganger de samme som bidrar til en økende delingskultur, men det betyr på ingen måte at disse bidrar til et fritt og åpent internett på rettferdige og bærekraftige vilkår.  

