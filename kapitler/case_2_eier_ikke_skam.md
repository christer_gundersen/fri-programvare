(må skrives om)
# Plateselskapene eier ikke SKAM!
De internasjonale plateselskapene eier ikke NRK serien SKAM men de styrer rettighetene til musikken som brukes i serien. Dette gjør at de nå kan tvinge frem geoblokkering av serien slik at den bare kan vises i Norge.
Skam er en nettbasert dramaserie som handler om livet til en rekke ungdommer på Hartvig Nissens skole i Oslo. Det har hittil kommet ut tre sesonger i serien som nå også har økende popularitet i utlandet. Den første episoden av Skam er en av de mest sette enkeltepisodene på NRK TV (nett-tv) noensinne og i gjennomsnitt har nettsiden 1,2 millioner unike brukere per uke og mer enn en million personer strømmer de ukentlige episodene.

Serien bruker musikk fra flere norske artister med den konsekvens at disse artistene får en helt unik profilering – også i utlandet. Et eksempel er låten «5 fine frøkner» som gjorde et voldsomt byks på Youtube etter den ble brukt i serien, låten passert også 10 millioner avspillinger på Spotify i desember 2016. Artisten Gabrielle Leithaug jublet selvsagt og hennes manager, Lars Kåre Hustoft, omtalte dette som hyggelig julegave i så sent som desember 2016. IFPI Norge og plateselskapene på sin side klarer ikke helt å se fordelen med at deres artister får denne typen gratis reklame.

## Musikkrettigheter skaper problemer
Det første tegnet til problemer kom allerede i november 2016 da NRK ble tvunget til å nekte teksting av serien til engelsk på grunn av musikkrettigheter. Dette skapte en storm på Twitter som endte i et opprop som fikk 2500 underskrifter. Serien kunne altså vises i utlandet via nettet, men ikke tekstes.

Sist uke tok saken en ny vending når NRK mottok krav fra IFPI Norge om umiddelbar geoblokkering av serien slik at den bare kan vises i Norge. IFPI er foreningen for de internasjonale plateselskapene og deres datterselskap i Norge. IFPI skal jobbe på vegne av artistene, men man kan virkelig spørre seg om de gjør det i denne saken.

Dette har selvsagt skapt engasjement hos mange som elsker serien og som mener det er viktig at den vises utenfor Norge. Med den enorme oppslutningen SKAM har fått i utlandet er det vanskelig å unngå å tenke på serien som en god eksportartikkel. Jeg synes denne Facebook kommentaren til Anne Siri Koksrud Bekkelund oppsummerer dette fortreffelig.
Skam er jo vår beste eksportartikkel siden trelast, og bygger relasjoner med Kina bedre enn offentlig pisking av Dalai Lama ville gjort. Samtidig sprer serien solide norske verdier som likestilling, girl power, homo-rettigheter og ungdomsfylla! Noen. Må. Gjøre. Noe. Nå. – Anne Siri Koksrud Bekkelund

## Creative Commons løser problemet
Den gode nyheten er at det finnes en løsning på dette problemet når NRK nå jobber med en ny sesong av SKAM. Den digitale delingskulturen er i dag godt utviklet. Denne delingskulturen bygger på at musikk og andre kilder blir underlagt det som kalles en fri lisens. Den mest brukte av disse er Creative Commons. Denne lisensen gir alle som ønsker det lov til å gjenbruke musikk, bilder, film og tekst uten å spørre om lov, men under gitte forutsetninger. Tillatelsen for å gjenbruke har opphavsmannen gitt på forhånd ved å bruke denne lisensen. Flere av de mest brukte CC lisensene tillater også kommersiell gjenbruk.

## Creative Commons lisens på Urørt?
NRK P3, som produserer SKAM, driver også nettstedet Urørt.no. Urørt er et nettsted hvor uetablerte norske artister og band kan promotere musikken sin, de beste blir også spilt på NRK radio. Ved å gi artistene mulighet til å lisensiere musikken med Creative Commons på Urørt.no kan NRK skape en unik mulighet for de artistene som ønsker å bidra til den globale delingskulturen. Samtidig vil NRK på sin side få mulighet til å bruke musikken med den forutsetning at opphavsmannen blir kreditert. Artister som ønsker det burde selvsagt få lov å legge ut musikk på en lukket lisens.

NRK som tross alt er finansiert med lisenspenger fra fellesskapet burde her tørre å tenke nytt. Målet må være at SKAM skal nå så mange som mulig og når musikkrettigheter står i veien for dette må man ganske enkelt komme opp med en løsning som gir maksimal eksponering av serier som produseres med midler fra fellesskapet.

