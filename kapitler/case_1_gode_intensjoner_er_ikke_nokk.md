# Satellittbilder for å redde regnskogen
Klima- og miljødepartementet (KMD) inngikk i september 2020 en kontrakten på 400 millioner kroner med Kongsberg Satellite Services (KSAT), Airbus og Planet. Målet med prosjektet er at man gjennom høyoppløselige satellittbilder får bedre innsyn i hva som skjer i de tropiske landene for å jobbe enda bedre med å redusere ødeleggelsen av verdens uvurderlige regnskoger. KMD har hatt sterkt fokus at disse bildene skal kunne gjenbrukes av andre og at dataene skal være en ressurs som er åpent tilgjengelig globalt. 

Dette har noen veldig spennende og potensielt katalytiske konsekvenser: 
* Den åpne lisensen gir andre nasjoner og uavhengige organisasjoner tilgang til disse svært kostbare dataene og de kan skape nye tjenester for sine innbyggere basert på de samme bildene.  
* Forskere over hele verden som jobber med å få frem ny kunnskap om regnskogen for mulighet til å sikre bedre kvalitet i sitt arbeid og resultater uten å måtte betale for tilgang til denne typen data. Dette gir også forskere mulighet til å sammenstille data fra flere åpne kilder i sin forskning, noe som    
* I mange områder spiller media en helt avgjørende rolle for å avdekke ulovlig hogst eller forurensing av regnskogen. Journalister og mediebedrifter i relevante områder får nå tilgang til de samme dataene med samme kvaliteten som KMD og deres samarbeidspartnere.   
* Dette gir også en nytt våpen for mindre lokalsamfunn når de går til kamp mot multinasjonale selskaper som driver uetiske i søken etter profitt når de produserer soya, palmeolje og andre råvarer.

Så hva er problemet? [beskrive utfordringen]
