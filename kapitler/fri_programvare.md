# Fri programvare og fri kultur (tidlig draft)
Fri programvare er i bruk over alt, du finner fri programvare i telefonen, bilen din, TV-en og kjøleskapet ditt. Fri programvare komponenter gjør det mulig for utviklere å bygge videre på arbeidet til andre utviklere for å levere bedre produkter raskere og til lavere pris. Linux, Apache webserver, Android, Firefox og WordPress er eksempler på fri programvare prosjekter du kanskje har hørt om.
Fri programvare representerer også en metode som er kraftig fordi den reduserer hindringene for samarbeid og deling, slik at folk kan spre og forbedre prosjekter raskt. Det gir også organsiasjoner og enkeltmennesker mer kontroll sammenlignet med lukket programvare. For eksempel har en virksomhet som bruker fri programvare muligheten til å ansette noen til å gjøre tilpassede forbedringer av programvaren, i stedet for å stole utelukkende på en beslutning fra en leverandør som tilbyr bruk av lukket programvare.

Fri programvare bygger på lang akademiske tradisjonen med åpenhet og deling. Etter hvert som teknologien har utviklet seg over de siste 30-40 årene, har det blitt enklere lettere å dele kildekoden, data, forskningsresultater, åpne læringsressurser og fritt kulturelt innhold. I takt med at flere har blitt brukere av internett, har den frie globale digital almenningen vokst. Internett er i seg selv tuftet i stor grad på fri programvare, og forholdet mellom det frie og åpne internett og fri programvare har vært svært viktig siden begynnelsen av 1990-tallet.

## Copyright
Copyright sikrer rettighetene til en opphavsperson som har skapt for eksempel programvare, et bilde, film eller musikk. Man kan endre og distribuere et kreativt verk utelukkende med tillatelse fra opphavsretts innehaveren. Copyright eksisterer for å beskytte rettighetene til utvikleren, kunstnere og andre skapere, slik at de kan kontrollere hvordan deres arbeid brukes av andre. I de fleste land gjelder Copyright automatisk i det øyeblikket arbeidet blir skapt.
I Fri programvare bevegelsen er det mange som ønsker å dele og kunne samarbeide med andre. Ved å bruke en fri programvarelisens på kildekode du publiserer, gir du andre rettighet til å laste ned, studere, endre og dele din kode.

## Det er ikke fri programvare uten en fri lisensen
En ekte fri programvarelisens er en som tillater bred bruk, modifisering og deling av koden, uten begrensninger. Tvetydige eller vage lisenser som ikke eksplisitt gir disse rettighetene til gjenbruk er problematiske fordi de er åpne for tolkning. Hvis du ikke er helt sikker på at du kan overholde forpliktelsene til en programvarelisens, bør du sannsynligvis ikke bruke programvaren. Hvis det skulle oppstå en juridisk utfordring, ønsker du ikke å bekymre deg for resultatet.
De mest populære lisensene er MIT, GPL og Apache. Den beste måten å unngå å lure på om en bestemt lisens er trygg å bruke, er å velge en av disse generelt aksepterte lisenser. De har blitt vurdert og gjennomgått av juridiske eksperter og andre i fri programvare miljøet.             

## Fri kultur                  
Fri kultur er en sosial bevegelse som fremmer menneskers frihet til å dele, gjenbruke, modifisere og distribuere sitt eget eller andres kreative verk. Fri Kultur regulerer deling gjennom lisenser som Creative Commons. Disse er i stor grad tuftet på de samme prinsippene som fri programvare men med utgangspunkt i musikk, film, kunst og kultur. 

Med sin filosofi rundt fri utveksling av ideer, er Fri kultur på mange måter inspirert av Fri programvare bevegelsen og filosofien til Richard Stallman og menneskene i Free Software Foundation.

En av de som var med å formet Fri kultur som begrep Lawrence Lessig, beskrev relasjonen til Stallman slik i boken «Fri kultur»:
«Inspirasjonen til tittelen og mye av argumentet i denne boken kommer fra arbeidet til Richard Stallman og Free Software Foundation. Faktisk, da jeg leste Stallmans egne tekster på nytt, spesielt essayene i Free Software, Free Society, innser jeg at alle de teoretiske innsiktene jeg utvikler her, er innsikter som Stallman beskrev for tiår siden. Man kan dermed godt argumentere for at dette verket «kun» er et avledet verk. Jeg godtar kritikken, hvis det faktisk er kritikk.»
Det er viktig å presisere at Fri kultur i stor grad er bygget på tanken om verdien av eierskap og at man som forfatter, musiker eller kunstner ikke på noen måte mister eierskap over sitt verk ved å bruke frie lisenser som Creative Commons.

I boken «Fri kultur» beskriver Lessig det slik:
Og som Stallman, tror jeg dette er verdier fra vår fortid som må forsvares i vår fremtid. En fri kultur har vært vår fortid, men vil bare være vår fremtid hvis vi endrer retningen vi følger akkurat nå. På samme måte som Stallmans argumenter for fri programvare, treffer argumenter for en fri kultur på forvirring som er vanskelig å unngå, og enda vanskeligere å forstå. En fri kultur er ikke en kultur uten eierskap.
