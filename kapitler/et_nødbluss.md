# Et nødbluss på vegne av vårt digitale demokrati!  

Alle større utfordringer vi som samfunn står overfor har en digital dimensjon. Dette gjelder alle sektorer inkludert utdanning, samferdsel, landbruk, forsvar og selvsagt klima og miljø. Samtidig er samfunnskritiske institusjoner innenfor byråkratiet, regjering, storting og domstoler i dag helt avhengig av sin digitale infrastruktur for å løse sine samfunnsoppdrag. 

Det er med andre ord ingen deler av vårt samfunn som i dag ikke berøres helt grunnleggende av teknologi i en eller annen form. Vi må derfor ta et steg tilbake å erkjenne at våre liv digitaliseres på alle nivåer. Som enkeltmennesker, lokalsamfunn, nasjon og demokrati. 

## Digitale utfordringer
Til tross for at det digitale nå er så viktig på så mange områder, er den digitaliserte delen av våre liv på ingen måte underlagt de samme retningslinjene og normene som samfunnet ellers. Dette har over tid skapt et virtuelt landskap med svært alvorlige utfordringer, som nå er i ferd med å slå inn over flere deler av vårt samfunn som en digital tsunami.

## Digital mistillit
Det er en økende grad av mistillit til digitale plattformer fordi den delen av vårt samfunn mangler helt grunnleggende demokratiske kjøreregler. Begrepet “privatlivets fred” er i ferd med å dø som et resultat av digitaliseringen. Som brukere har vi ikke kontroll over våre personlige data, og er helt frarøvet et digitalt privatliv. Dette dels fordi samfunnet rundt oss samler inn data om oss og våre bevegelser, samtidig som noen globale aktører samler data om alle. Denne utviklingen er preget av kaos og manglende regulering. 

## Digitale monopolister og supermakter
Våre digitale flater styres i stor grad av en håndfull aktører som har monopol på sine områder, noe som gjør at Norge og andre nasjoner mister råderett over det som i praksis fungerer som samfunnskritisk infrastruktur. 

Det digitale gapet mellom offentlige institusjoner og multilaterale organisasjoner på den ene siden og de nevnte selskapene på den andre, er i ferd med å bli så stort at vi i fremtiden kan bli helt avhengig av de nevnte selskapene for å løse store nasjonale og globale utfordringer. 

Forholdet mellom nasjoner er også svært ubalansert. Kina, Russland og USA satser tungt innenfor flere digitale felt, mens Norge og andre europeiske land gjør lite. De sistnevnte blir derfor fanget i en kryssild mellom de digitale supermaktene og de store globale selskapene. Russlands hacking av det Norske stortinget for noen måneder siden hvor stortinget var avhengig av Microsoft for å løse problemet er bare et lite frampek på hva som kan komme.

## Svake digitale offentlige institusjoner 
Offentlig sektor i Norge og andre land har svært få virkelig store satsinger på digital infrastruktur hvor de selv sitter med eierskap til teknologien. Dette fører til at viktige nasjonale institusjoner mister kontroll over kjernesystemer, mens en eller noen få private aktører sitter med “nøkkelen til safen”.

Et resultat av manglende digital satsning over flere tiår, er at tiltak for å regulerer det digitale domenet og markedsaktører ofte bærer preg av manglende kompetanse blant embetsverk og politikere. Dette fører til at nye lover og reguleringer ikke fungerer etter intensjonene.

Vår altfor sterke avhengighet av markedet gjør at vi ofte baserer strategier, og i Norges tilfelle stortingsmeldinger, på den feilaktige oppfatningen at digital utvikling i stor grad handler om utvikling av et marked, og at offentlige institusjoner kun skal være kunder i dette markedet.

## Vi må tegne kartet på nytt
Med utgangspunkt i den betydningen det digitale har for så mange sektorer og områder, burde vi ikke i da i større grad sikre at det digitale domenet er tuftet på de samme verdiene og normene som gjelder ellers i samfunnet? I så fall, hvordan ønsker vi at vårt digitale samfunnet skal se ut? Og like viktig, hvilken form bør den teknologiske utviklingen ta for å bidra til å bringe oss i den retningen vi ønsker? 

## Styrking av offentlige institusjoner
Vi må endre vårt grunnsyn på det digitale rommet slik at det også inkluderer sosiale, kulturelle og demokratiske verdier, og vi må endre syn på det offentliges rolle i digitaliseringen. Som nasjon må vi gjenvinne vår digitale selvråderett for å skape et virtuelt samfunn som omfavner våre demokratiske verdier og sikrer grunnleggende rettigheter og sosial rettferdighet. Vi som innbyggere må sikres demokratisk innflytelse også i den digitale dimensjonen. 

Offentlige institusjoner styrkes også digitalt gjennom økt internasjonalt samarbeid som legger til rette for en global delingskultur som er rettferdig og demokratisk, der grunnleggende friheter og rettighet er beskyttet, og hvor sterke offentlige institusjoner fungerer i allmennhetens interesse. 

## Støtte opp under den digitale allmenningen
En viktig forutsetning for å skape en god debatt rundt dette temaet, er at vi slutter å snakke om teknologi som en kraft med en forutbestemt retning som er definert av noen få selskaper i Silicon Valley. Vi har har kommet til et punkt der de skadelige effektene er klart større enn verdien disse tjenestene og selskapene gir oss som brukere. I fremtiden må nettverkseffekten som genereres av og med oss brukere komme allmennheten til gode – på brukernes premisser. Vi må også sikre brukerne en reell mulighet til digital sporløs ferdsel på alle plattformer. 

Denne utfordringen strekker seg helt ned på infrastrukturnivå. Norsk offentlig sektor må i årene som kommer bidra til å desentralisere vår teknologiske infrastruktur for å redusere avhengigheten av noen få teknologileverandører. Det vil ha en kostnad, men samtidig bidra til å styrke den digitale allmenningen. Det vil på sikt bidra til å opprettholde våre demokratiske tradisjoner og mangfold også i det digitale domenet. 

Offentlig sektor må ta ansvar for å sikre et digitalt mangfold. Norge må derfor definere en helhetlig strategi for offentlig delingskultur (“open policy”) som gjelder for alle offentlige virksomheter. Dette betyr kort fortalt at all teknologi, data, forskningsresultater og digitalt innhold som finansieres av offentlige virksomheter må deles helt åpent. Dette vil bidra til å stimulere til et bærekraftig næringsliv og en sunn og levedyktig digital allmenning. 

## Digitale løsninger på globale utfordringer 
Mange er i dag bekymret for utviklingen i det digitale landskapet, men hvilke praktiske konsekvenser har denne egentlig for de store utfordringene vi som samfunn står overfor?  

La oss ta klima og miljøkampen som et eksempel, vår tids største globale utfordring. Det er hevet over enhver tvil at forskning, teknologi, åpne data, kunstig intelligens og andre digitale ressurser kommer til å være helt avgjørende for å nå de målene vi som verdenssamfunn har satt oss for å løse klimakrisen de neste tiårene. 

Det digitale i denne sammenhengen kan dreie seg om alt fra batteriteknologi, algoritmer basert på kunstig intelligens, åpne værdata eller satellittbilder av regnskogen i Amazonas som dokumenterer avskoging. Det handler om digitale ressurser som helt konkret påvirker vår evne til å bevare miljøet for fremtidige generasjoner og som burde deles som digitale goder som er tilgjengelig for alle som ønsker å bidra i denne globale kampen.

Spørsmålet i dette eksemplet blir da som følger: Mener vi som samfunn at den digitale dimensjonen av klimakampen kan overlates til en gruppering bestående av de store digitale plattform monopolene i en slags digital stillingskrig med Kina, USA og Russland? Det kan føles som et retorisk spørsmål, men men det er i alle høyeste grad en reell problemstilling vi som samfunn aktivt må ta stilling til - og det haster!








