# Digital sirkulær økonomi og det grønne skiftet

De alvorlige konsekvensene av klimaendringene er i dag tydelige over hele verden. Ifølge forskere vil temperaturen fortsette å øke drastisk selv om vi kutter klimagassutslipp betydelig de neste 30-40-årene. Dette vil få store konsekvenser på alle kontinenter og for alle land, noe som gjør kampen for klima og miljø til den viktigste i vår tid. 

FNs klimapanelet publiserte en rapport i 2018 som viste forskjellen på å begrense oppvarmingen til 1,5 grader, sammenlignet med det gamle målet på 2 grader. Forskjellige viser seg å være dramatisk. En halv grad kan med andre ord bety mye for mange. 

Teknologi og digitale løsninger vil være helt avgjørende for å nå de målene vi som verdenssamfunn har satt oss for å løse de store utfordringene. Dette handler i praksis om digitale ressurser som helt konkret påvirker vår evne til å bevare miljøet for fremtidige generasjoner. 

Det grønne teknologiske skiftet må komme fort, og ny teknologi og innovative løsninger må gjøres tilgjengelig for så mange som mulig, så raskt som mulig. For å løse disse enorme utfordringen i tide, trenger vi å skape en global digital delingskultur i kampen for miljøet. En åpen tilnærming i den digitale delen av miljøkampen kan bidra til å øke samarbeidet og innovasjon rundt miljøvennlige teknologiske alternativer. Dette vil også sikre at land under utvikling får tilgang til ny teknologi raskere. 

Flere organisasjoner har lenge bidratt til denne globale delingskulturen innenfor forskning og teknologisektoren gjennom det som kalles “open access” eller en “open policy”. Bill and Melinda Gates Foundation er en av organisasjonene som var tidlig ute. I deres “open access policy” krever de bruk av åpne lisenser for all forskning og alle publikasjoner, inkludert underliggende data. 

## Hva betyr teknologi i den taktile klimakampen?
Det digitale i denne sammenhengen dreier seg om alt fra batteriteknologi, algoritmer basert på kunstig intelligens, åpne værdata og satellittbilder av regnskogen i Amazonas som dokumenterer avskoging. Teknologi satt sammen med tverrfaglige forskningsdata kan bidra til å dokumentere hvilke problemer vi må løse, og hvordan vi kan løse dem.

Også innenfor områder der det digitale tradisjonelt ikke spiller en viktig rolle, vil ny og innovativ teknologi være helt avgjørende. Et eksempel er landbruk, hvor mangel på vann er et stort problem i mange land. Teknologi for å bruke mindre vann og for å gi avlingen riktig mengde vann til riktig tid, er et fagområde hvor det er gjort store fremskritt de siste årene. Det forskes mye rundt bruk av kunstig intelligens for å utvikle et ressurseffektivt landbruk, som i tillegg til å konsumere mindre ressurser, vil gi sunnere og bedre avlinger. Forskere ved Wageningen Universitet i Nederland er blant de som har utviklet ny og spennende teknologi innenfor dette området.

Et annet eksempel er industri som bruker mye stål, sement og plast, noe som forårsaker en betydelig mengde utslipp. Dette gjelder både konstruksjon for industri og private boliger. For å redusere denne typen utslipp må man utvikle teknologi som gjør det enkelt og kostnadseffektivt å bygge med andre materialer. The Wiki House project er et eksempel hvor arkitekter har utviklet en metode som gir lave utslipp, og hvor over 90% av ressursene som brukes i bygningskonstruksjon kan resirkuleres. “The Wiki House project” deler også alle sine spesifikasjoner helt åpent under en Creative Commons lisens, noe som gjør at andre kan bygge et hus, en låve eller et kontorbygg med deres teknologi og metode. Bruk av kunstig intelligens for å utvikle nye konstruksjonsmetoder, hvor man 3D-printer hus med materialer som er lett tilgjengelig i et geografisk område, er et annet eksempel som vil bidra til å begrense de totale utslippene. 

Norge er på noen områder å regne som et foregangsland med teknologi som bidrar til å sikre miljøet både i Norge og verden ved at data og teknologi deles fritt

## Yr.no deler åpne globale værdata 
Yr.no er et annet eksempel. Mange av oss bruker Yr.no daglig for å få oppdaterte værmeldinger for Norge. Noe ikke alle er klar over, er at Meteorologisk institutt også tilbyr data som dekker andre deler av verden. Det er faktisk bare omtrent halvparten av trafikken på Yr som kommer fra Norge. Resten av brukerne er spredt over 200 land globalt. Totalt kan man på Yr.no søke etter værvarsler for over 11 millioner steder i verden, noe som gjør tjenesten til en digital ressurs som for eksempel kan bidra til at bønder i afrikanske land er bedre i stand til å planlegge innhøsting. Sør-Afrika er et av landene hvor Yr allerede brukes aktivt. 

En annen spennende mulighet med dataene fra Yr.no er at de har en lisens som tillater kommersiell gjenbruk. Dette gjøre det i praksis mulig for en “startup” i Kenya å bruke data fra Yr.no til å lage en app eller en annen type applikasjon som de så selger som et kommersielt produkt. 

## VIPS
VIPS er en varslingstjeneste som gir informasjon om fare for angrep og skader av sykdommer, skadedyr og ugress. VIPS er utviklet av Norsk institutt for bioøkonomi som baserer sine varsler på værdata, observasjoner i felt, skadeterskler og modeller. Plattformen er bygget på gjenbruk av andre kilder samtidig som teknologien deles fritt. VIPS er på mange måter som tatt ut av læreboka for digital sirkulær økonomi.

Digital delingskultur - det eneste alternativet
Begge disse norske prosjektene er eksempler på at gode teknologiske løsninger eller datasett med høy kvalitet ofte er komplisert og kostnadskrevende å utvikle. Det er derfor svært lite sannsynlig at land under utvikling har mulighet til å finansiere denne typen data og tjenester selv. Global digital delingskultur rundt klima og miljø er derfor den eneste måten vi kan sikre at de beste teknologiske løsningene blir tilgjengelig for de som trenger dem mest. 

Når nye løsninger utvikles med offentlige midler, og offentlig finansiert prosjekter resulterer i teknologi som kan bidra til å redde klimaet, må denne teknologien være åpent tilgjengelig. Vi har rett og slett ikke råd eller tid til å havne i en situasjon hvor en eller noen veldig få private aktører sitter med enerett på teknologi som kan bidra til å løse de globale klimautfordringen. 

Utgangspunktet for digitale delingskulturen gir på måter enda flere muligheter for gjenbruk sammenlignet med den “fysiske sirkulære økonomien”. I steden for å hente ut verdi, tilfører man verdi gjennom hver deling og vært derivat som et resultat av gjenbruk. 

Skal vi nå de ambisiøse klimamålene må vi utvikle en digital delingskultur og vi må legge til rette for globalt tverrfaglig samarbeid.  


